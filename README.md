# HA TP-Link manual connection

This is how I fixed my TP-Link HS100 smart plugs after the latest update that stopped the local API working with Home Assistant.

## Initial sadness

Okay so I'll give you some background on what was happening with my plugs and what I did to eventually fix it, hopefully it will match up with what you're experiencing.

So a few months ago I updated everything and my plugs just stopped connecting to home assistant. They were always showing as unavailable and the logs were showing failure to connect errors constantly.

I assumed something had changed in the TP-Link component and thought I'd just live with it for a bit because I couldn't be bothered to downgrade. But the problem stayed over many versions.

I eventually wiped my raspberry pi and re-installed everything from scratch, tried multiple versions of HA including one that I knew worked with them previously and they all were doing the same thing.

I went to the Kasa app and saw that none of the plugs were listed in there anymore. I tried setting them up without creating an account but it wouldn't work. Each plug would connect to the wifi but the app wouldn't recognise it unless I signed in with a Kasa cloud account. HA still couldn't see anything, same errors, but the app could control them.

So I tried downloading the same Python module that HA uses to talk to the plugs and tried talking to them directly, which just threw and error and died. So then I scanned the ports and realised that a bunch of them were no longer showing as open anymore.

I started wondering wether TP Link had locked everything to the cloud, or wether they'd perhaps made the local API only work with the device that had setup the plug in the first place.

So I assumed the latter, and thought it must be possible to pair them without the app with some messing around if the local API worked originally.

## Stuff you will need

I did some googling and came accross a few things. The first thing you'll need is this wonderful python script, so save this somewhere:

https://github.com/softScheck/tplink-smartplug/blob/master/tplink_smartplug.py

Open up a terminal/command prompt/whatever and navigate to where you stored that python script. Before you start make sure in your Kasa app, that you have told it to forget all your devices and signed out of your Kasa account.

## Setup plugs

Then do the following steps for each of the TP-Link plugs you have:

1) Get one of your plugs and hold down the top button for 12 seconds (the little one, not the green button on the front), that will factory reset it. After a few moments it will start flashing orange and green.

2) On your computer, scan the available wifi networks and find the one that says something along the lines of "TP-Link plug", this is the wifi hotspot the plug creates when it wants to be configured. Connect to that network, and make sure that your computer stays connected to it in case it comes up complaining there's "no internet on this network" or something.

3) Once connected, the first thing we need to do is tell the plug it's name. Go to your terminal and run this command `python3 tplink_smartplug.py -t 192.168.0.1 -j '{"system":{"set_dev_alias":{"alias":"YOUR_PLUGS_NAME_HERE"}}}'`.

4) Now we can tell the plug to connect to the wifi with this command `python3 tplink_smartplug.py -t 192.168.0.1 -j '{"netif":{"set_stainfo":{"ssid":"YOUR_WIFI_SSID_HERE","password":"YOUR_WIFI_PASSWORD_HERE","key_type":3}}}'`

5) After a few seconds, the wifi light on your plug should be steady green showing that it is new connected to your network. Go back to step 1 for the next plug you want to configure.


At this point, when my computer was connected back to the actual wifi, I was able to send commands to my plugs with this script such as:

`python3 tplink_smartplug.py -t PLUG_IP_ADDRESS -c on` and `python3 tplink_smartplug.py -t PLUG_IP_ADDRESS -c off`

## More sadness

The plugs were responding how they used to, but HA still couldn't see them :\. This might not be a problem on HomeAssistant OS - try restarting home assistant **before** you do any of this and see if the plugs have reconnected. If they haven't, then this is my best guess for how to fix it:

I did some more digging, and I saw that the ARP table on my HomeAssistant system was not being populated correctly with the plugs. It was showing the plugs mac addresses and various other details as "unknown".

I was at a loss, and decided to tell the arp table manually where and what these devices were.

You can do that by running this command on your Home Assistant box's terminal for each plug you have `arp -s PLUG_IP_ADDRESS PLUG_MAC_ADDRESS`.

But that won't last if the HA box reboots. So I took all of those commands and put them all in a bash script that looked something like this, but obviously with different mac addresses:

```
#!/bin/sh

/usr/sbin/arp -s 192.168.1.45 B0:BE:76:79:E3:9E
/usr/sbin/arp -s 192.168.1.46 B0:BE:76:79:E3:9E
/usr/sbin/arp -s 192.168.1.47 B0:BE:76:79:E3:9E
/usr/sbin/arp -s 192.168.1.48 B0:BE:76:79:E3:9E
/usr/sbin/arp -s 192.168.1.49 B0:BE:76:79:E3:9E

exit 0
```

Then I set my pi to run that script on boot up, after the network connection has been established.

For the sake of convenience I just ran that bash script rather than rebooting, and then ran the `arp` command to check that all my plugs IP addresses had mac addresses associated with them. They were all showing up so I restarted home assistant and gave it a few minutes for the TP-Link component to think about life, and then the plugs all showed up and worked again :)